﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ReactiveUI;

namespace RxUiTests
{
    /// <summary>
    /// Interaction logic for Person.xaml
    /// </summary>
    public partial class Person : TreeViewItem, IViewFor<ViewModel.Person>
    {
        public Person()
        {
            InitializeComponent();
            //Items.Clear();
            this.Bind(ViewModel, vm => vm.Name, v => v.Header);
            this.OneWayBind(ViewModel, vm => vm.Children, v => v.ItemsSource);
            this.Bind(ViewModel, vm => vm.IsSelected, v => v.IsSelected);
            //this.WhenAnyValue(v => v.ViewModel.Children)
            //    .Subscribe(c => ItemsSource = c);
            //this.ItemsSource = ViewModel.Children;
            //this.OneWayBind(ViewModel, vm => vm.Children, v => v.ItemsSource);
        }

        object IViewFor.ViewModel
        {
            get { return ViewModel; }
            set { ViewModel = value as ViewModel.Person; }
        }

        public ViewModel.Person ViewModel { get; set; }
    }
}
