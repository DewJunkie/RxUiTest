﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;

namespace RxUiTests.ViewModel
{
    public class MainWindow: ReactiveObject
    {
        public MainWindow()
        {
            People = new ReactiveList<Person>()
            {
                ChangeTrackingEnabled = true
            };

            var lora = new Person() {Name = "Lora"};
            var duane = new Person() {Name = "Duane"};
            lora.Children.Add(duane);
            duane.Children.Add(new Person() {Name = "Bailey"});
            duane.Children.Add(new Person() { Name = "Daniel" });
            duane.Children.Add(new Person() { Name = "Meagan" });
            var alese = new Person() {Name = "Alese"};
            alese.Children.Add(new Person() { Name = "Irie" });
            People.Add(lora);
            People.Add(alese);

            AddChildCommand = ReactiveCommand.Create(() =>
            {
                if (SelectedPerson != null)
                {
                    SelectedPerson.Children.Add(new Person());
                }
                else
                {
                    People.Add(new Person() {Name = "new"});
                }
            });
        }

        [Reactive] public ReactiveList<Person> People { get; set; }

        [Reactive] public Person SelectedPerson { get; set; }

        public ReactiveCommand<Unit,Unit> AddChildCommand { get; set; }
    }
}
