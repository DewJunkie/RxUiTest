﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;

namespace RxUiTests.ViewModel
{
    public class Person: ReactiveObject
    {
        public static readonly Serilog.ILogger Logger = Serilog.Log.ForContext<Person>();
        public Person()
        {
            Children = new ReactiveList<Person>()
            {
                ChangeTrackingEnabled = true
            };

            this.WhenAnyObservable(vm => vm.Children.ItemChanged)
                .Subscribe(change =>
                {
                    Logger.Debug("{Name} {Child} changed {Property}", Name, change.Sender.Name, change.PropertyName);
                });
        }
        [Reactive] public string Name { get; set; }
        [Reactive] public ReactiveList<Person> Children { get; set; }
        [Reactive] public bool IsExpanded { get; set; }
        [Reactive] public bool IsSelected { get; set; }
    }
}
