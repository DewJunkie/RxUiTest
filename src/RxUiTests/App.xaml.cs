﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using ReactiveUI;
using Serilog;
using Serilog.Sinks.Network;
using Splat;
using ILogger = Serilog.ILogger;

namespace RxUiTests
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private static ILogger _logger;
        public App()
        {
            var loggerConfiguration = new LoggerConfiguration()
                .WriteTo.UDPSink(IPAddress.Loopback, 1337)
                .WriteTo.RollingFile("log.txt")
                .MinimumLevel.Verbose();

            Log.Logger = loggerConfiguration.CreateLogger();
            _logger = Log.Logger.ForContext<App>();

            _logger.Verbose("Started");
        }
        private void App_OnDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            _logger.Warning(e.Exception, "Unhandled exception");
        }
    }
}
