﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ReactiveUI;
using RxUiTests.ViewModel;
using Splat;

namespace RxUiTests
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IViewFor<ViewModel.MainWindow>
    {
        public MainWindow()
        {
            InitializeComponent();

            //Locator.CurrentMutable.Register(() => new Person(), typeof(IViewFor<ViewModel.Person>));
            ViewModel = new ViewModel.MainWindow();

            //this.OneWayBind(ViewModel, vm => vm.People, v => v.TreeViewPeople.ItemsSource);
            this.WhenAnyValue(v => v.ViewModel.People)
                .Subscribe(p => TreeViewPeople.ItemsSource = p);
            this.Bind(ViewModel, vm => vm.SelectedPerson, v => v.TreeViewPeople.SelectedItem);

            this.BindCommand(ViewModel, vm => vm.AddChildCommand, v => v.ButtonAdd);
        }

        object IViewFor.ViewModel
        {
            get { return ViewModel; }
            set { ViewModel = value as ViewModel.MainWindow; }
        }

        public ViewModel.MainWindow ViewModel { get; set; }

        private void ButtonTest_OnClick(object sender, RoutedEventArgs e)
        {
        }
    }
}
